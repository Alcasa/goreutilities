Utilities library for GoreLab members

Dependency for the FlowCytometryTools package.

Includes tools to work with:
(1) File dialogues
(2) Files
(3) Common plotting tasks
(4) Posting ipython notebooks on wordpress blogs

