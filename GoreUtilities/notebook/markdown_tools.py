'''
Created on Nov 27, 2013

@author: yonatanf
'''
import os
from profile import CLOUDSERVICE
if CLOUDSERVICE == 'dropbox':
    from dropbox_api import get_url
elif CLOUDSERVICE == 'gdrive':
    from gdrive_api import get_url

def get_nbviewer(path):
#     return 'http://nbviewer.ipython.org/urls/' + re.search('dl.*', path).group(0)
    return 'http://nbviewer.ipython.org/urls/' + path.strip('https://').strip('http://')

def get_bit(path):
    return 'bitbucket.org/eugene_yurtsev/wiki/raw/HEAD/' + path

def get_bd(path):
    out = os.path.join('/home/eyurtsev/Work/Dropbox/GoreLab/Projects/ProjectBlackDove/Analysis', path)
    return out

named_tag_repl = {
'db' : get_url,
'bit' : get_bit,
'bd' : get_bd,
'nbview': get_nbviewer
}

named_tag_priority = {
'db': 0,
'bit' : 0,
'bd' : -1,
'nbview': 1
}

extensions = ['extra', 'meta', 'mathjax']
#extensions = ['extra', 'meta']

def repl_decorator(tag):
    def actualDecorator(repl):
        def wrapper(m, **kwargs):#, **kwargs
            n = len(tag)
            match  = m.group(0)[n+2:-(n+3)] #match without the tag brackets
            if (tag == 'db') & (CLOUDSERVICE == 'gdrive'):
                return repl(match, **kwargs) #, **kwargs
            else:
                return repl(match)
        return wrapper
    return actualDecorator

def sub_tag(tag, repl, string, count=0, flags=0, **kwargs):#, **kwargs
    """
    Parameters
    ---------------
    
    tag : str
    repl : callable
    
    """
    from re import subn
    start = r'\[' + tag + r'\]'
    content = r'.*'                                
    end = r'\[/' +tag + r'\]'
    pattern = start+content+end
    new, n = subn(pattern, lambda x : repl_decorator(tag)(repl)(x, **kwargs), string, count, flags)#, **kwargs
    return new

def sub_tags(md_txt, tag_repl=named_tag_repl, tag_priority=named_tag_priority, **kwargs):
    '''
    Operate on strings found between tags in markdown txt.
    Tags appear in the txt as: [tag]string to be opperated on[\tag]. 
    
    tag_repl : dict
        key = tag string
        val = callable to operate on strings found between tags.
    tag_priotiy : dict
        determines the order by which the tag will be operated on.
        key = tag string
        val = priority values. Lower values will be operated on first.
    '''
    tags = tag_repl.keys()
    sorted_tags = sorted(tags, key=lambda t:tag_priority[t])
    txt = md_txt
    for tag in sorted_tags:
        txt = sub_tag(tag, tag_repl[tag], txt, **kwargs)
    return txt

def process_md_meta(meta):
    for k,val in meta.iteritems():
        val_list = []
        for v in val: val_list += [vi.strip() for vi in v.split(',')]
        if len(val_list) == 1 and k not in ['categories', 'tags']:
            meta[k] = val_list[0]
        else:
            meta[k] = val_list
    return meta

def to_html(md_txt, extensions=extensions, preprocess=True, **kwargs):
    
    import markdown
    md = markdown.Markdown(extensions=extensions)
    if preprocess:
        preprocessed = sub_tags(md_txt, **kwargs)
    html = md.convert(preprocessed)
    return html, md

def get_meta_from_md(path, extensions=extensions):
    """ Gets the meta data from the indicated md file. """
    import markdown
    md = markdown.Markdown(extensions=extensions)
    with open(path, 'rb') as f:
        md_txt = f.read().decode('utf-8')
    txt = md.convert(md_txt)
    meta = md.Meta
    return meta

def update_meta_in_md_txt(md_txt, new_meta, extensions=extensions, new_path=None):
    """
    """
    import markdown
    md = markdown.Markdown(extensions=extensions)
    txt = md.convert(md_txt)
    new_meta = process_md_meta(new_meta)
    updated_meta = process_md_meta(dict(md.Meta))
    updated_meta.update(new_meta)

    output_str = ""
    for k, v in updated_meta.items():
        if isinstance(v, (list, tuple)):
            v_str = ', '.join(v)
        else:
            v_str = '{}'.format(v)
        output_str += "{}: {}\n".format(k, v_str)
    output_str += '\n'.join(md.lines)

    return output_str

if __name__ == '__main__':
    print 'nothing'
    #import os
    #path = '/home/yonatanf/Dropbox/markdown_tst.md'
    #f = open(path, 'rb')
    #md_txt = f.read().decode('utf-8')
    #f.close()
     #
    #html, md = to_html(md_txt)      
    #f = open(os.path.splitext(path)[0] +'.html'  , 'wb')
    #f.write(html)
    #f.close()  
