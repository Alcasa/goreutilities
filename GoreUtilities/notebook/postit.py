#!/usr/bin/env python
"""
Command line interface to post markdown files
and to make markdown files from ipython notebooks
"""
from wordpress_api import post_from_md
import os
import argparse
from argparse import RawTextHelpFormatter
from ipynb_to_markdown import produce_md_from_notebook, update_notebook_meta_from_md
from profile import CLOUDSERVICE
if CLOUDSERVICE == 'gdrive':
    from gdrive_api import move_to_gdrive

def parse_input():
    """
    Examples
    -------------
    To post a wordpress post:
    >> python postit.py my_post.md
    >> python postit.py my_post.ipynb -> Creates a folder called my_post. Puts the md file inside of it & posts it.
    >> python postit.py -n my_post.ipynb -> Does not post but does everything else.
    """
    epilog = parse_input.__doc__
    parser = argparse.ArgumentParser(epilog=epilog, formatter_class=RawTextHelpFormatter)
    parser.add_argument("files", help="markdown file(s) to post", metavar="FILE", nargs='+', default=None)
    parser.add_argument("-n", "--no-post", action="store_false", dest="post", default=True, help="Don't post md file")
    parser.add_argument("-q", "--quiet", action="store_false", dest="verbose", default=True,
                      help="Don't print status messages to stdout")
    return parser.parse_args()

if __name__ == '__main__':
    args = parse_input()
    for file_path in args.files:
        ext = os.path.splitext(file_path)[1]
        file_path = file_path.replace(".ipynb", "") + '.ipynb'
        notebook = ext == '.ipynb' # Then ipython notebook

        if notebook:
            md_path, md_content = produce_md_from_notebook(file_path,
                    open_tags="[db]", close_tags="[/db]")
            if args.post:
                if CLOUDSERVICE == 'gdrive':
                    keys = move_to_gdrive(md_path)
                    post_from_md(md_path, keys=keys)
                elif CLOUDSERVICE == 'dropbox':
                    post_from_md(md_path)
                update_notebook_meta_from_md(file_path, md_path)
        else: # Then '.md file'
            if args.post:
                if CLOUDSERVICE == 'gdrive':
                    keys = move_to_gdrive(file_path)
                    post_from_md(file_path, keys=keys)
                elif CLOUDSERVICE == 'dropbox':
                    post_from_md(file_path)
